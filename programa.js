//se crea una clase
class Triqui {

    constructor() {

        this.matriz = [
            ["A", "B", "C"],
            ["D", "E", "F"],
            ["G", "H", "I"]
        ];

        this.dibujarTablero();
        this.definirJugador();
    }
//funcion para dibujar el tablero
    dibujarTablero() {

        //Crear un objeto que referencie a la división tablero
        let miTablero = document.getElementById("tablero");
        //utilizando el cliclo for se crean las casillas
        for (let i = 0; i < 9; i++) {
            miTablero.innerHTML = miTablero.innerHTML +
                "<input type='button' id='casilla" + (i + 1) + "' class='casilla' onclick='miTriqui.realizarJugada()'>";
            //cada 3 casillas se hace un salto de linea
            if ((i + 1) % 3 == 0) {
                miTablero.innerHTML = miTablero.innerHTML + "<br>";
            }
        }
    }
//funcion para elegir quien comienza la partida
    definirJugador() {
        let n;
        //Crear un objeto que referencie a la división turno
        let miTurno = document.getElementById("turno");
        //se elige que jugador va a comenzar la partida utilizando aleatorios
        n = Math.round(Math.random() + 1);
        if (n === 1) {
            this.turno = "X";
        } else {
            this.turno = "O";
        }
        miTurno.innerHTML = "Es el turno de: " + this.turno;
    }
//funcion que realiza la jugada
    realizarJugada() {

        let miElemento = event.target;
//condicion que verifica si la casilla esta vacia
        if (!(miElemento.value === "X" || miElemento.value === "O")) {
            miElemento.value = this.turno;
            this.modificarMatriz(miElemento.id);
//condicion que verifica si se hace triqui y si asi es, crear un objeto que referencie a la división resultado e informa quien gano
            if(this.verificarTriqui()===true){
                document.getElementById("resultado").innerHTML="¡"+this.turno+" ES EL GANADOR!";

            }else{
                this.cambiarTurno();
                if(this.verificarTriqui()===false){
                    document.getElementById("resultado"),innerHTML="AUN NO HAY GANADOR";
                }else{
                    this.cambiarTurno();
                }
            }
        }else{
            //se crea un objeto que referencie a la división error e informa que la casilla no es valida
            document.getElementById("error").innerHTML="La casilla esta llena";
        }
        
    }
 //se crea una funcion la cual va a verificar si se hace triqui
    verificarTriqui() {

        let triqui = false;
//ciclo for para buscar triqui en las filas
        for (let fila = 0; fila < 3; fila++) {
            if (this.matriz[fila][0] === this.matriz[fila][1] && this.matriz[fila][0]=== this.matriz[fila][2]) {
                triqui = true;
                return triqui;
                }
        }
//ciclo for para buscar triqui en las columnas
        for (let columna = 0; columna < 3; columna++) {
            if (this.matriz[0][columna] === this.matriz[1][columna] && this.matriz[0][columna]=== this.matriz[2][columna]) {
                triqui = true;
                return triqui;
            }
        }
//condicion que verifica si hay triqui en las diagonales
        if ((this.matriz[0][0] === this.matriz[1][1] && this.matriz[0][0]=== this.matriz[2][2]) || 
        (this.matriz[0][2] === this.matriz[1][1] && this.matriz[0][2] === this.matriz[2][0])){
            triqui = true;
            return triqui;
        }

        return triqui;
    }
    modificarMatriz(id) {

        for(let i=0; i<3; i++){  
            if(id === this.matriz[0][i]){
                    this.matriz[0][i] = this.turno;                
            }
        }
        for(let i=0; i<3; i++){  
            if(id === this.matriz[1][i]){
                    this.matriz[1][i] = this.turno;                
            }
        }
        for(let i=0; i<3; i++){  
            if(id === this.matriz[2][i]){
                    this.matriz[2][i] = this.turno;                
            }
        } 
    }
    cambiarTurno() {
//Crear un objeto que referencie a la división turno
        let miTurno = document.getElementById("turno");
//condicion para intercambiar los turnos
        if (this.turno === "X") {

            this.turno = "O";
        } else {
            this.turno = "X";
        }
        miTurno.innerHTML = "Es el turno de: " + this.turno;
    }

    iniciar(){

        this.matriz=[
            ["casilla1", "casilla2", "casilla3"],
            ["casilla4", "casilla5", "casilla6"],
            ["casilla7", "casilla8", "casilla9"]
        ];
        //ciclo para limpiar las casillas
        for(let i=0;i<9;i++){
            let campo =document.getElementById("casilla"+(i+1));
            campo.value=" "; 
        }
//se limpia la division de resultado
        let r =document.getElementById("resultado");
        r.innerHTML=" ";
//se limpia la division de errores
        let c = document.getElementById("error");
        c.innerHTML=" ";
    }
}